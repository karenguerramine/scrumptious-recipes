from dataclasses import field
from datetime import datetime
from tkinter import CASCADE
from unittest.util import _MAX_LENGTH
from django.db import models


# Create your models here.
class Recipe(models.Model):
    name = models.CharField(max_length=125)
    author = models.CharField(max_length=100)
    description = models.TextField()
    image = models.URLField(null=True, blank=True)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    def __str__(self):
      return self.name + "by" + self.author
  
  
class Measure(models.Model):
    name = models.CharField(max_length=30, unique=True)
    abbreviation = models.CharField(max_length=30, unique=True)
  
    def __str__(self):
      return self.name + "-"  + self.abbreviation
    
  
class FoodItem(models.Model):
  name = models.CharField(max_length=100)
  
  def __str__(self):
    return self.name
  
class Ingredient(models.Model):  
  amount = models.FloatField()
  recipe = models.ForeignKey(Recipe,related_name="ingredients", on_delete=models.CASCADE)
  measure = models.ForeignKey(Measure, models.PROTECT)
  food = models.ForeignKey(FoodItem,models.PROTECT)
  
  def __str__(self):
    return self.recipe + "=" + self.amount + " " + self.measure + " " + self.food
     
class Step(models.Model):
  recipe = models.ForeignKey(Recipe,related_name="steps",on_delete=models.CASCADE)
  order = models.SmallIntegerField()
  directions = models.CharField(max_length=300)
  
  def __str__(self):
    return f"{self.recipe} {self.order} {self.directions}"

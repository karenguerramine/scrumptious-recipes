from django.contrib import admin
from recipes.models import Recipe

class RecipeAdmin(admin.ModelAdmin):
  pass

class MeasureAdmin(admin.ModelAdmin):
  pass

class FoodItemAdmin(admin.ModelAdmin):
  pass

class IngredientAdmin(admin.ModelAdmin):
  pass

class StepAdmin(admin.ModelAdmin):
  pass

admin.site.register(Recipe)

